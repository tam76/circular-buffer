object CircularBuffer {

  class MyQueue(size: Int) {
    private var queue = new Array[Option[String]](size)
    private var pos = 0
    private val remove = () => queue = queue.tail :+ null

    def enqueue(element: String) {
      if (pos < size) queue(pos) = Some(element)
      else if (pos > size + 2) {
        System.err.println("Queue is really full")
        System.exit(1)
      }else {
        remove()
        queue(size -1) = Some(element)
      }
      pos += 1
    }

    def list() {
      if (pos >= size)
        println("There are too many characters stored, please remove something to continue")
      println(queue.toList) 
    }

    def dequeue() {
      remove()
      pos = if (pos < size) pos - 1 else size - 1
    }
  }
  
  def main(args: Array[String]) {
    while(true) {
      println("Input size of buffer:")
      val size = Console.readInt
      if(size <= 20 && size > 0) {
        println("Circular Buffer using commands A (Enqueue), R (Dequeue), L (list), and Q (quit)")
        println("Enqueue example: A <input> <input*>")
        val buffer = new MyQueue(size)
        while(true) {
          println("Input command:")
          val char = Console.readLine
          if(char == "Q") {
            println("Good bye user, see you next time")
            System.exit(1)
          } else if (char == "L") buffer.list()
          else if (char == "R") buffer.dequeue()
          else if (char.startsWith("A"))
            char.split("\\s+").tail.foreach(buffer.enqueue(_))
          else println(char + " is a wrong command, input in (A, L, R, Q).")
        }
      } else
        println("Please keep buffer size between 1 and 20")
    }
  }
}
